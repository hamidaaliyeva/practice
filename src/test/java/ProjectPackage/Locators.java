package ProjectPackage;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class Locators {

    //    @FindBy(xpath = "//div[@id=\'menu\']/ul/li[4]")
    @FindBy(xpath = "//div[@id='menu']/ul/li/a[contains(text(), 'Tester’s Hub')]")
    WebElement testersHub;

    //    @FindBy(xpath = "//div[@id='menu']/ul/li[4]/div/ul/li[1]")
    @FindBy(xpath = "//a/span[contains(text(), 'Demo Testing Site')]")
    WebElement demoTestingSite;

    public void clickDemoTestingSite(Actions actions) {
        actions.moveToElement(testersHub).moveToElement(demoTestingSite).click().build().perform();
    }

    @FindBy(xpath = "//div[@class='price_column '][1]/ul/li[@class='price_footer']")
    List<WebElement> firstColumn;

    @FindBy(xpath = "//div[@class='price_column '][2]/ul/li[@class='price_footer']")
    List<WebElement> secondColumn;

    @FindBy(xpath = "//div[@class='price_column '][3]/ul/li[@class='price_footer']")
    List<WebElement> thirdColumn;

    public int firstColumnSize() {
        return firstColumn.size();
    }

    public int secondColumnSize() {
        return secondColumn.size();
    }

    public int thirdColumnSize() {
        return thirdColumn.size();
    }

    @FindBy(xpath = "//a/span[contains(text(), 'DatePicker')]")
    WebElement dataPicker;

    public void clickDataPicker(Actions actions) {
        actions.moveToElement(testersHub).moveToElement(demoTestingSite).moveToElement(dataPicker).click().build().perform();
    }

    @FindBy(css = "#post-2661 > div.twelve.columns > div > div > div.single_tab_div.resp-tab-content.resp-tab-content-active > p > iframe")
    WebElement iframeDate;

    @FindBy(xpath = "//input[@id='datepicker']")
    WebElement inputDatePicker;

    @FindBy(xpath = "//td[@class=' ui-datepicker-days-cell-over  ui-datepicker-today']")
    WebElement today;

    @FindBy(xpath = "//a[@class='ui-datepicker-next ui-corner-all']")
    WebElement nextMonth;

    public String selectDate(WebDriver webDriver) {
        inputDatePicker.click();
        String todayNumber = today.getText();
        String xpath = "//td[@data-handler='selectDay']/a[(text() = '" + todayNumber + "')]";
        nextMonth.click();
        WebElement dayAfterMonth = webDriver.findElement(By.xpath(xpath));
        dayAfterMonth.click();
        return inputDatePicker.getAttribute("value");
    }

    @FindBy(xpath = "//a/span[text()='Progress Bar']")
    WebElement progressBar;

    public void clickProgressBar(Actions actions) {
        actions.moveToElement(testersHub).moveToElement(demoTestingSite).moveToElement(progressBar).click().build().perform();
    }

    @FindBy(css = "#post-2671 > div.twelve.columns > div > div > div.single_tab_div.resp-tab-content.resp-tab-content-active > p > iframe")
    WebElement iframeProgressBar;

    @FindBy(xpath = "//button[@id='downloadButton']")
    WebElement startDownloadButton;

    @FindBy(xpath = "//div[@id='dialog']/div[@class='progress-label']")
    WebElement complete;


    public String startDownload(WebDriverWait webDriverWait) {
        startDownloadButton.click();
        webDriverWait.until(ExpectedConditions.textToBe(By.xpath("//div[@id='dialog']/div[@class='progress-label']"), "Complete!"));
        return complete.getText();
    }
}
