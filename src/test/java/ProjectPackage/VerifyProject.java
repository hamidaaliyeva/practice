package ProjectPackage;

import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class VerifyProject {
    ProjectWebdriver projectWebdriver = new ProjectWebdriver();
    Locators locators = PageFactory.initElements(projectWebdriver.webDriver, Locators.class);
    Actions actions = new Actions(projectWebdriver.webDriver);
    WebDriverWait webDriverWait = new WebDriverWait(projectWebdriver.webDriver, 20);

    @Test
    public void verifyColumnNumbers() {
        try {
            locators.clickDemoTestingSite(actions);
            int actual = 6;
            Assert.assertEquals(locators.firstColumnSize(), actual);
            Assert.assertEquals(locators.secondColumnSize(), actual);
            Assert.assertEquals(locators.thirdColumnSize(), actual);
        } finally {
            projectWebdriver.webDriver.close();
        }
    }

    @Test
    public void verifyDate() {
        try {
            locators.clickDataPicker(actions);
//            webDriverWait.until(ExpectedConditions.visibilityOf(locators.iframeDate));
            projectWebdriver.webDriver.switchTo().frame(locators.iframeDate);
            String expectedDate = locators.selectDate(projectWebdriver.webDriver);
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MM/dd/yyyy");
            Date date =simpleDateFormat.parse(expectedDate);
            Assert.assertTrue(expectedDate.equals(simpleDateFormat.format(date)));
        } catch (ParseException e) {
            e.printStackTrace();
        } finally {
            projectWebdriver.webDriver.close();
        }
    }

    @Test
    public void verifyDownload(){
        try {
            locators.clickProgressBar(actions);
            projectWebdriver.webDriver.switchTo().frame(locators.iframeProgressBar);
            String expectedResult = locators.startDownload(webDriverWait);
            Assert.assertEquals(expectedResult, "Complete!");
//            System.out.println(expectedResult);
        }finally {
            projectWebdriver.webDriver.close();
        }
    }
}
