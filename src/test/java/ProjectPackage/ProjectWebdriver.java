package ProjectPackage;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.concurrent.TimeUnit;

public class ProjectWebdriver {
    WebDriver webDriver;

    public ProjectWebdriver() {
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\Hamida\\Documents\\chromedriver_win32\\chromedriver.exe");
        webDriver = new ChromeDriver();
        webDriver.get("https://www.globalsqa.com/");
        webDriver.manage().window().maximize();
        webDriver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
    }
}
